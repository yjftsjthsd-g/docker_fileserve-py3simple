FROM alpine AS base
LABEL maintainer "Brian Cole <docker@brianecole.com>"
RUN apk update && apk add python3

FROM base AS app
RUN ["/bin/rm", "-rf", "/media", "/var/cache/apk", "/mnt", "/usr/lib/python3.6/site-packages", "/usr/lib/python3.6/distutils", "/usr/lib/python3.6/ensurepip"]
RUN ["mkdir", "-p", "-m", "755", "/srv/http"]

# Set the working directory to /app
WORKDIR /srv/http

# Copy the current directory contents into the container at /app
COPY . /srv/http

# Make port 8000 available to the world outside this container
EXPOSE 8000

# Run the trivial python http server
USER nobody:nobody
STOPSIGNAL 9
HEALTHCHECK --interval=15s --timeout=3s CMD /usr/bin/wget --spider http://localhost:8000/
ENTRYPOINT ["python3", "-m", "http.server", "8000"]
