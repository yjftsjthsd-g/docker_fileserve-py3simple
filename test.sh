#!/bin/sh
IMAGE_ID="$1"
CONTAINER_ID="$(docker run -d -p 80:8000 "$IMAGE_ID")"
sleep 1
wget -O test_output localhost/
docker container stop "$CONTAINER_ID"
diff test_output index.html
exit $?
